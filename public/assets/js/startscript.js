// Initializes Simple Effects

$(function(){

	$('.tooltip').powerTip();
	$('.cs-scroller').niceScroll({
		cursorcolor: '#805930',
		cursorborderradius: 0,
		cursorborder: '0',
		autohidemode: false,
	});

	$('.scroll-textarea').keydown(function(){
		$(this).getNiceScroll().resize();
	});

	$('#flyout').animate({
		top: '20%',
		opacity: '1'
	}, 500).delay(1500).animate({
		top: '60%',
		opacity: 0
	}, function(){
		$(this).remove();
	});

	$('#message-dropdown').click(function(){
		$(this).next().slideToggle();
	});

	$('#message-dropdown-content').click(function(){
		$(this).slideToggle();
	});
});

