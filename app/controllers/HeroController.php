<?php

use herotamer\Exception\HerotamerException,
	herotamer\Services\Hero\HeroService;

class HeroController extends Controller{

	protected $hero;

	public function __construct(HeroService $hero)
	{
		$this->hero = $hero;
	}
		
	public function choose()
	{
		return View::make('herotamer.hero.choose');
	}

	public function finalChoose($class)
	{
		$this->hero->chooseStartClass($class);
		return Redirect::to('account/dashboard');
	}

}