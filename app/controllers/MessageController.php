<?php

use herotamer\Services\Message\MessageService,
	herotamer\Services\Message\MessageNotAllowedException;

class MessageController extends AccountBaseController{
	
	protected $messageService;

	public function __construct(MessageService $messageService)
	{
		parent::__construct();
		$this->messageService = $messageService;
	}

	public function inbox()
	{
		$messages = $this->messageService->getInbox();
		return View::make('herotamer.messages.inbox', array(
			'messages' => $messages
		));
	}

	public function read($messageId)
	{
		$message = $this->messageService->getMessage($messageId);				
		return View::make('herotamer.messages.read', array(
			'message' => $message
		));
	}

	public function getWrite()
	{
		return View::make('herotamer.messages.write');
	}

	public function redirect($messageId)
	{
		$message = $this->messageService->getRedirectMessage($messageId);
		return View::make('herotamer.messages.write', array(
			'message' => $message
		));
	}

	public function reply($messageId)
	{
		$message = $this->messageService->getReplyMessage($messageId);
		return View::make('herotamer.messages.write', array(
			'message' => $message,
			'replyto' => $message->user->username,
		));
	}

	public function send()
	{
		$errors = $this->messageService->validateMessage(Input::all());
		if($errors !== true)
		{
			return Redirect::back()->withInput()->withErrors($errors);
		}
		else
		{
			Session::flash('modal' ,'Nachricht erfolgreich versendet');
			return Redirect::to('messages/inbox');
		}
	}

}