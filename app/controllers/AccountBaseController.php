<?php

use herotamer\Services\Message\MessageHandler;

class AccountBaseController extends Controller {

	public function __construct()
	{
		Event::fire('messages.refresh');
	}
}