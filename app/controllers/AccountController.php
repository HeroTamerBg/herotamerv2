<?php

use herotamer\Interfaces\UserRepositoryInterface;

class AccountController extends AccountBaseController{
	
	protected $user;

	public function __construct(UserRepositoryInterface $user)
	{
		$this->user = $user;
	}

	public function dashboard()
	{
		return View::make('herotamer.account.dashboard');
	}


}