<?php

use herotamer\Interfaces\UserRepositoryInterface,
	herotamer\Services\Registration\RegistrationFormValidator,
	herotamer\Services\Registration\RegistrationService,
	herotamer\Services\AccountAction\AccountActionService;

class HomeController extends Controller {

	protected $validator;
	protected $registrationService;
	protected $accountActionService;

	public function __construct(
		RegistrationFormValidator $validator, 
		RegistrationService $registrationService, 
		AccountActionService $accountActionService
	)
	{
		$this->validator = $validator;
		$this->registrationService = $registrationService;
		$this->accountActionService = $accountActionService;
	}

	public function home()
	{
		var_dump(Auth::check());
		return View::make('herotamer.home.index');
	}


	public function validateRegistration()
	{
		if($this->validator->with(Input::all())->passes())
		{
			$this->registrationService->register(Input::all());
			return View::make('herotamer.registration.success', array(
				'username' =>Input::get('username')
			));
		}
		else
		{
			return Redirect::back()->withInput()->withErrors($this->validator->errors());
		}
	}

	public function registration()
	{
		return View::make('herotamer.registration.registration');		
	}

	public function login()
	{
		if( $this->accountActionService->login(Input::all()) )
		{
			return Redirect::to('account/dashboard');
		}
		else
		{
			Session::flash('login_error','Logindaten falsch');
			return Redirect::back();
		}
	}

	public function logout()
	{
		if(!$this->accountActionService->isLoggedIn())
			return View::make('herotamer.notifications.generalerror', array(
				'message' => 'Du bist nicht eingeloggt',
				'linkTo'  => 'home'
 			));		
		else
		{
			$this->accountActionService->logout();
			return Redirect::to('home');
		}
	}

}