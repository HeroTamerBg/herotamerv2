<?php

namespace herotamer\Repositories;
use herotamer\Interfaces\UserRepositoryInterface,	
	Illuminate\Database\Eloquent\Model;

class EloquentUserRepository implements UserRepositoryInterface{

	protected $user;

	public function __construct(Model $user)
	{
		$this->user = $user;
	}

	public function all(){}
	public function find($id){
		return $this->user->find($id);
	}

	public function create($input)
	{
		return $this->user->create(array(
			'username' => $input['username'],
			'password' => \Hash::make($input['password']),
			'email' => $input['email']
		));
	}

	public function usernameToId($username)
	{
		return $this->user->where('username', $username)->first()->id;
	}

}