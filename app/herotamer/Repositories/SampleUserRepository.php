<?php

namespace herotamer\Repositories;
use herotamer\Interfaces\UserRepositoryInterface,	
	Illuminate\Database\Eloquent\Model;

class SampleUserRepository implements UserRepositoryInterface{

	protected $user;

	public function __construct(Model $user)
	{
		$this->user = $user;
	}
	
	public function all()
	{
		return "Return all Users";
	}

	public function find($id)
	{
		return "Got User with ID " . $id;
	}

	public function create($input)
	{
		return $this->user->create(array(
			'username' => $input['username'],
			'password' => \Hash::make($input['password']),
			'email' => $input['email']
		));
	}

}