<?php

namespace herotamer\Services\Hero;
use herotamer\Services\Auth\AuthRepositoryInterface;

class HeroService{
	
	protected $auth;

	public function __construct(
		AuthRepositoryInterface $auth
	)
	{
		$this->auth = $auth;
	}

	public function chooseStartClass($class)
	{
		$classes = array('warrior' => 1, 'mage' => 2, 'bow' => 3);
		if(!array_key_exists($class, $classes))
			throw new HerotamerException('Gecheatet wird nicht.', 'hero/choose');
		$this->auth->user()->heroes()->attach($classes[$class]);
	}

}