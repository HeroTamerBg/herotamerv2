<?php

namespace herotamer\Services\Registration;
use herotamer\Services\Validation\AbstractLaravelValidator;

class RegistrationFormValidator extends AbstractLaravelValidator{

	protected $rules = array(
		'username' => 'required|alpha_num|between:4,12|unique:users',
		'email' => 'required|email|unique:users',
		'password' => 'required|min:8|confirmed',
		'referrer' => 'between:4,12|alpha_num'
	);

	protected $messages = array(
		'username.unique' => 'Dieser Username existiert bereits',
		'email.unique' => 'Diese E-Mail Adresse existiert bereits',
	);





}