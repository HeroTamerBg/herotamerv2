<?php

namespace herotamer\Services\Registration;
use herotamer\Interfaces\UserRepositoryInterface,
	herotamer\Services\Auth\AuthRepositoryInterface,
	Exception;


class RegistrationService{

	protected $userRepository;
	protected $authRepository;

	public function __construct(
		UserRepositoryInterface $userRepository, 
		AuthRepositoryInterface $authRepository
	)
	{
		$this->userRepository = $userRepository;
		$this->authRepository = $authRepository;
	}

	public function register($input)
	{
		$user = $this->userRepository->create($input);	
		$this->authRepository->loginUsingId($user->id);
	}




}