<?php

namespace herotamer\Services\Registration;
use Illuminate\Support\ServiceProvider;

class RegistrationServiceProvider extends ServiceProvider{
	
	public function register()
	{
		$this->app->bind('herotamer\Services\Registration\RegistrationFormValidator', function(){
			return new RegistrationFormValidator($this->app['validator']);
		});
	}

}