<?php

namespace herotamer\Services\Message;
use herotamer\Services\Validation\AbstractLaravelValidator;

class MessageFormValidator extends AbstractLaravelValidator{
	
	protected $rules = array(
		'to' => 'required|exists:users,username',
		'subject' => 'required|between:3,30',
		'message' => 'required|between:2,500'
	);

}	