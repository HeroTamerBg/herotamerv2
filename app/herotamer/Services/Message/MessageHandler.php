<?php

namespace herotamer\Services\Message;
use herotamer\Services\Auth\AuthRepositoryInterface,
	View;


class MessageHandler{

	protected $view;
	protected $message;
	protected $auth;

	public function __construct(MessageRepositoryInterface $message, AuthRepositoryInterface $auth)
	{
		$this->auth = $auth;
		$this->message = $message;
	}

	public function handle()
	{
		View::share('unread_messages', $this->message->getUnread($this->auth->user()->id));
	}

}