<?php

namespace herotamer\Services\Message\Exception;
use Illuminate\View\Environment;


class MessageExceptionHandler
{

	protected $view;

	public function __construct(Environment $view)
	{
		$this->view = $view;
	}

	public function handle($e)
	{
		return $this->view->make('herotamer.herotamer.error')->with(array(
			'message' => $e->getMessage(),
			'uri' => $e->getUri()
		));
	}

}