<?php

namespace herotamer\Services\Message\Exception;
use Exception;

class MessageNotAllowedException extends Exception{

	protected $uri;

	public function __construct($msg, $uri)
	{
		parent::__construct($msg);
		$this->uri = $uri;
	}

	public function getUri()
	{
		return $this->uri;
	}

}