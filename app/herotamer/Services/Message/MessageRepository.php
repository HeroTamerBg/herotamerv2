<?php

namespace herotamer\Services\Message;
use Illuminate\Database\Eloquent\Model;

class MessageRepository implements MessageRepositoryInterface{

	public $message;

	public function __construct(Model $message)
	{
		$this->message = $message;
	}

	public function getAllByReceiver($receiverId)
	{
		return $this->message->where('to', $receiverId)->orderBy('id', 'desc');
	}

	public function getById($messageId)
	{
		return $this->message->find($messageId);
	}

	public function getUnread($userId)
	{
		return $this->message
			->where('recipiant_flag', 0)
			->where('to', $userId)
			->count();
	}

	public function createMessage($input)
	{
		$this->message->create($input);
	}


}