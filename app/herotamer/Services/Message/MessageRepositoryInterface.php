<?php

namespace herotamer\Services\Message;

interface MessageRepositoryInterface{

	public function getAllByReceiver($receiverId);
	public function getById($messageId);

}