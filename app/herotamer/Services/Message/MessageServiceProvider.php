<?php

namespace herotamer\Services\Message;
use Illuminate\Support\ServiceProvider,
	herotamer\Exception\HerotamerExceptionHandler,
	herotamer\Exception\HerotamerException,
	Event;


class MessageServiceProvider extends ServiceProvider{

	public function register()
	{
		$this->app->bind('herotamer\Services\Message\MessageRepositoryInterface', function(){
			return new MessageRepository(new Message);
		});

		$this->app->bind('herotamer\Services\Message\MessageFormValidator', function(){
			return new MessageFormValidator($this->app['validator']);
		});

		$this->app['herotamer.services.message.exception'] = $this->app->share(function(){
			return new MessageExceptionHandler($this->app['view']);
		});

		$this->registerEvents();
	}

	protected function registerEvents()
	{
		Event::listen('messages.refresh', 'herotamer\Services\Message\MessageHandler');		
	}


}