<?php

namespace herotamer\Services\Message;
use Eloquent;

class Message extends Eloquent{

	protected $guarded = array();

	public function user()
	{
		return $this->belongsTo('herotamer\Entities\User', 'from');
	}

	public function recipiant()
	{
		return $this->belongsTo('herotamer\Entities\User', 'to');
	}

}