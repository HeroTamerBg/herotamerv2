<?php

namespace herotamer\Services\Message;
use herotamer\Services\Auth\AuthRepositoryInterface,
	herotamer\Interfaces\UserRepositoryInterface,
	herotamer\Exception\HerotamerException,
	Event;


class MessageService{

	protected $message;
	protected $user;
	protected $validator;
	
	public function __construct(
		AuthRepositoryInterface $auth,
		MessageRepositoryInterface $message,
		MessageFormValidator $validator,
		UserRepositoryInterface $userRepository
	)
	{
		$this->user = $auth->user();
		$this->message = $message;
		$this->validator = $validator;
		$this->userRepository = $userRepository;
	}

	public function getInbox()
	{
		return $this->message->getAllByReceiver($this->user->id)->get();
	}

	public function getMessage($messageId)
	{	
		$message = $this->message->getById($messageId);
		if(!$this->canRead($message))
		{		
			throw new HerotamerException('Tut uns leid, diese Nachricht ist nicht für dich bestimmt', 'messages/inbox');
		}
		
		if($message->recipiant_flag == 0)
		{
			$message->recipiant_flag = 1;
			$message->save();
		}
		Event::fire('messages.refresh');			
		return $message;

	}

	public function getRedirectMessage($messageId)
	{
		$message = $this->message->getById($messageId);
		if(!$this->canRead($message))
		{
			throw new HerotamerException('Tut uns leid, aber diese Nachricht kannst du nicht weiterleiten', 'messages/inbox');
		}

		$message->subject = 'Re: '. $message->subject;
		$message->text = 'Original Nachricht von: '. $message->user->username . "\r\n ----------------------------------------------------- \r\n" . $message->text;
		return $message;
	}

	public function getReplyMessage($messageId)
	{
		$message = $this->message->getById($messageId);
		if(!$this->canRead($message))
			throw new HerotamerException('Tut uns leid, aber auf diese Nachricht kannst du nicht antworten', 'messages/inbox');
		$message->subject = 'Aw: '. $message->subject;
		return $message;			
	}

	protected function canRead($message)
	{
		if($message->from != $this->user->id AND $message->to != $this->user->id)
			return false;
		return true;
	}

	public function validateMessage($input)
	{
		if(!$this->validator->with($input)->passes())
		{
			return $this->validator->errors();
		}
		else
		{
			$this->message->createMessage(array(
				'from' => $this->user->id,
				'to'   => $this->userRepository->usernameToId($input['to']),
				'subject' => $input['subject'],
				'text'  => trim($input['message'])
			));
			return true;
		}
	}
}