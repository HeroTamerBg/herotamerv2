<?php

namespace herotamer\Services\AccountAction;
use herotamer\Services\Auth\AuthRepositoryInterface;

class AccountActionService{

	protected $auth;
	
	public function __construct(AuthRepositoryInterface $auth)
	{
		$this->auth = $auth;
	}

	public function login($input)
	{
		$username = $input['login_email'];
		$password = $input['login_password'];

		$credentials = array(
			'email' => $username, 
			'password' => $password
		);

		if($this->auth->attempt($credentials))
			return true;
		return false;
	}

	public function logout()
	{
		$this->auth->logout();
	}

	public function isLoggedIn()
	{
		return $this->auth->check();
	}

}