<?php

namespace herotamer\Services\Auth;

interface AuthRepositoryInterface{
	
	public function user();
	public function loginUsingId($id);
	public function attempt($data);
	public function check();
	public function logout();


}