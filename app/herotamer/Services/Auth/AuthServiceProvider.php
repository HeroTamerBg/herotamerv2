<?php

namespace herotamer\Services\Auth;
use Illuminate\Support\ServiceProvider,
	Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider{

	public function register()
	{
		$this->app->bind('herotamer\Services\Auth\AuthRepositoryInterface', function($app){
			return new AuthRepository($this->app->make('auth'));
		});
	}

}