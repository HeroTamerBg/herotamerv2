<?php

namespace herotamer\Services\Auth;

class AuthRepository implements AuthRepositoryInterface{

	protected $auth;

	public function __construct($auth)
	{
		$this->auth = $auth;
	}
	
	public function user()
	{
		return $this->auth->user();
	}

	public function loginUsingId($id)
	{
		return $this->auth->loginUsingId($id);
	}

	public function attempt($data)
	{
		return $this->auth->attempt($data);
	}

	public function check()
	{
		return $this->auth->check();
	}

	public function logout()
	{
		return $this->auth->logout();
	}

}