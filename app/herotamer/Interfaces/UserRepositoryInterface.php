<?php

namespace herotamer\Interfaces;

interface UserRepositoryInterface{

	public function all();
	public function find($id);
	public function create($data);

}