<?php

namespace herotamer\Interfaces;

interface ValidationInterface{

	public function with(array $input);
	public function passes();
	public function errors();

}