<?php

namespace herotamer\Entities;
use Illuminate\Auth\UserInterface,
	Illuminate\Auth\Reminders\RemindableInterface,
	Eloquent;

class User extends Eloquent implements UserInterface, RemindableInterface {

	protected $fillable = array('username', 'email', 'password');

	public function heroes()
	{
		return $this->belongsToMany('herotamer\Services\Hero\Hero');
	}

	public function getAuthIdentifier()
	{
		return $this->getKey();
	}


	public function getAuthPassword()
	{
		return $this->password;
	}

	public function getReminderEmail()
	{
		return $this->email;
	}

}