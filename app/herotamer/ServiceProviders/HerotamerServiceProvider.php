<?php

namespace herotamer\ServiceProviders;
use Illuminate\Support\ServiceProvider,
	View,

	herotamer\Exception\HerotamerExceptionHandler,
	herotamer\Exception\HerotamerException,
	herotamer\Repositories\EloquentUserRepository,
	herotamer\Entities\User;

class HerotamerServiceProvider extends ServiceProvider{

	public function register()
	{
		$this->app->bind('herotamer\Interfaces\UserRepositoryInterface', function(){
			return new EloquentUserRepository(new User);
		});	

		$this->app['herotamer.exception'] = $this->app->share(function(){
			return new HerotamerExceptionHandler($this->app['view']);
		});		
	}

	public function boot()
	{
		$app = $this->app;
		$this->app->error(function(HerotamerException $e) use ($app){
			return $app['herotamer.exception']->handle($e);
		});
	}


}