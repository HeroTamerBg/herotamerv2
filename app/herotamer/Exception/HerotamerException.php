<?php

namespace herotamer\Exception;
use Exception;

class HerotamerException extends Exception{

	protected $uri;

	public function __construct($msg, $uri)
	{
		parent::__construct($msg);
		$this->uri = $uri;
	}

	public function getUri()
	{
		return $this->uri;
	}

}
