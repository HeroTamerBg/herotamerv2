<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

use herotamer\Entities\User;

Route::filter('hero_chosen', function(){

	if(!Auth::check())
		return Redirect::to('registration');
	if(Auth::user()->heroes()->count() == 0)
		return Redirect::to('hero/choose');

});

Route::group(array('before' => 'hero_chosen'), function()
{
	Route::get('messages/inbox', 'MessageController@inbox');
	Route::get('messages/inbox/{messageId}', 'MessageController@read');
	Route::get('messages/write', 'MessageController@getWrite');
	Route::post('messages/send', 'MessageController@send');
	Route::get('messages/redirect/{messageId}', 'MessageController@redirect');
	Route::get('messages/reply/{messageId}', 'MessageController@reply');

	Route::get('account/dashboard', 'AccountController@dashboard');
});

Route::group(array('before' => 'auth'), function(){

	Route::get('hero/choose', 'HeroController@choose');
	Route::get('hero/choose/{cls}', 'HeroController@finalChoose');

});


Route::get('registration', 'HomeController@registration');
Route::post('registration', 'HomeController@validateRegistration');

Route::post('account/login', 'HomeController@login');
Route::get('account/logout', 'HomeController@logout');

Route::get('home', 'HomeController@home');