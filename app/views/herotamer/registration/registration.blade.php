@extends('herotamer.herotamer.index')

@section('top_content')
	<div id="game-logo-container" class="fl">
		<div id="game-logo"></div>
	</div>
	<div id="game-description" class="fl">
		<h1>Steige ein in das Abenteuer von HeroTamer</h1>
		<p class="textleft">
			HeroTamer ist großartig. Registriere Dich noch heute
			und sichere dir Deinen kostenlosen BJ von Adriana Lima
		</p>
		<p class="textleft">
			Unser Geheimtipp! Geh weg.
		</p>
	</div>
	<div class="fl">
		@include('herotamer.partials.login')
	</div>
	<div class="clearfix"></div>
@stop

@section('bottom_content')
	<div id="book-content" class="registration-form">
		<div id="page-headline"></div>
		{{ Form::open(array('url' => 'registration')) }}
		<div class="input-line" style="margin-bottom: 25px;">
			<div class="tooltip" data-powertip="Mit diesem Namen bist du im Spiel aktiv! <br />4-12 Zeichen, Alphanumerisch" <?php echo $errors->has('username') ? 'style="margin-top: 7px;"' : ''; ?>></div>

			{{ Form::label('username', 'Dein Spielername:') }}
			@if($errors->has('username'))
				<div class="registration-error">
					{{ Form::text('username') }}
					<div class="registration-error-text">{{ $errors->first('username') }}</div>
				</div>
			@else
				{{ Form::text('username') }}
			@endif
			<div class="clearfix"></div>
		</div>
		<div class="input-line" style="margin-bottom: 25px;">
			<div class="tooltip" data-powertip="Deine E-Mail Adresse ist für andere Spieler unsichtbar.<br/> Dein Aktivierungslink wird an diese E-Mail gesendet." <?php echo $errors->has('email') ? 'style="margin-top: 7px;"' : ''; ?>></div>			
			{{ Form::label('email', 'Deine E-Mail:') }}

			@if($errors->has('email'))
				<div class="registration-error">
					{{ Form::text('email') }}
					<div class="registration-error-text">{{ $errors->first('email') }}</div>
				</div>
			@else
				{{ Form::text('email') }}
			@endif
			<div class="clearfix"></div>				
		</div>
		<div class="input-line">
			
			<div class="tooltip" data-powertip="Ein sicheres Passwort enthält Zahlen, Buchstaben und Sonderzeichen. <br />Benutze keine leicht zu erratenden Passwörter. <br />Mindestens 8 Zeichen" <?php echo $errors->has('password') ? 'style="margin-top: 7px;"' : ''; ?>></div>			

			{{ Form::label('password', 'Dein Passwort:') }}
			@if($errors->has('password'))
				<div class="registration-error">
					{{ Form::password('password') }}
					<div class="registration-error-text">{{ $errors->first('password') }}</div>
				</div>
			@else
				{{ Form::password('password') }}
			@endif
			<div class="clearfix"></div>
		</div>
		<div class="input-line" style="margin-bottom: 25px;">

			{{ Form::label('password_confirmation', 'Passwort bestätigen:') }}
			@if($errors->has('password_confirmation'))
				<div class="registration-error">
					{{ Form::password('password_confirmation') }}
					<div class="registration-error-text">{{ $errors->first('password_confirmation') }}</div>
				</div>
			@else
				{{ Form::password('password_confirmation') }}
			@endif
			<div class="clearfix"></div>				
		</div>

		<div class="input-line" >
			<div class="tooltip" data-powertip="Trage den Benutzernamen von demjenigen ein der Dich geworben hat.<br />Ihr erhaltet beide großartige Boni im Spiel!" <?php echo $errors->has('referrer') ? 'style="margin-top: 7px;"' : ''; ?>></div>						
			{{ Form::label('referrer', 'Geworben von:') }}
			@if($errors->has('referrer'))
				<div class="registration-error">
					{{ Form::text('referrer') }}
					<div class="registration-error-text">{{ $errors->first('referrer') }}</div>
				</div>
			@else
				{{ Form::text('referrer') }}
			@endif			
			<div class="clearfix"></div>
		</div>

		<div id="confirmation-box">
			<div class="confirmation-block" style="margin-right: 30px;">
				<div class="bullet">1</div>
				<p class="confirmation-paragraph" style="width: 133px">Fülle alle Felder ordnungsgemäß aus</p>
				<div class="clearfix"></div>
			</div>
			<div class="confirmation-block" style="margin-right: 30px;">
				<div class="bullet">2</div>
				<p class="confirmation-paragraph" style="width: 133px">
					Ich bestätige die AGB's und Spielregeln <br />
				</p>
				<div class="fl" style="padding-left: 10px; padding-top: 5px;">
					Ja
					{{ Form::checkbox('agb') }}
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="confirmation-block">
				<div class="bullet">3</div>
				<p class="confirmation-paragraph" style="width: 133px">Klicke auf den Button und los geht's!</p>
				<div class="clearfix"></div>
			</div>
			{{ Form::submit('', array('id' => 'registration-submit')) }}						
		</div>
		{{ Form::close() }}
	</div>
@stop