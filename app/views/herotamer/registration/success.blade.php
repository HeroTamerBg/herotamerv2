@extends('herotamer.herotamer.index')

@section('top_content')
	<div id="registration-success-box">
		<div id="registration-success-name-box">{{ $username}}</div>
		<div id="game-logo"></div>
	</div>
@stop

@section('bottom_content')
	<div id="registration-success-bottom">
		<div id="registration-success-step-shadow"></div>
		<h1 class="strong-h" style="margin-bottom: 20px;">Keine E-Mail erhalten?</h1>

		<div class="bullet" style="margin-top: 10px;">1</div>
		<p class="confirmation-paragraph" style="width: 248px;">
			Falls Du keinen Aktivierungslink erhalten hast, schaue gegebenenfalls in deinem Spam-Ordner nach
		</p>

		<div class="bullet" style="margin-top: 10px; margin-left: 30px;">2</div>
		<p class="confirmation-paragraph" style="width: 220px;">
			Solltest du keine E-Mail bekommen haben, klicke auf diesen Link um erneut
			deinen Aktivierungslink anzufordern.
		</p>
		<div class="clearfix"></div>
		<div class="bullet" style="margin-top: 10px;">3</div>
		<p class="confirmation-paragraph" style="width: 248px;">
			Wenn nichts hilft, kontaktiere unseren Support. Deine Anfrage wird schnellstmöglich
			bearbeitet.
		</p>
		<div class="bullet" style="margin-top: 10px;margin-left: 30px;">!</div>
		<p class="confirmation-paragraph" style="width: 220px;">
			Sobald du aktiviert und eingeloggt bist, kannst du deine persönlichen Daten jederzeit ändern.
		</p>		
	</div>
@stop