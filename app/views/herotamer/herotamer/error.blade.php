@extends('herotamer.herotamer.index')

@section('bottom_content')
	<div id="book-content-ingame">
		<div id="page-headline"></div>
	
		<div id="message-ui">
			<div id="message-ui-error"></div>
			{{ $message }}
			<a id="message-ui-button" href="{{ URL::to($uri) }}">Zurück</a>
		</div>
	</div>
@stop