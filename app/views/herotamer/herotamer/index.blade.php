<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Welcome</title>
        @section('stylesheets')
            <link href="{{ asset('assets/css/reset.css') }}" rel="stylesheet" />
            <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
            <link href="{{ asset('assets/css/vendor/ui-herotamer/jquery-ui-1.10.3.custom.min.css') }}" rel="stylesheet" />
            <link href="{{ asset('assets/css/vendor/customscrollbar.css') }}" rel="stylesheet" />
            <link href="{{ asset('assets/css/vendor/customscrollbar.css') }}" rel="stylesheet" />
        @show
    </head>
    <body>
        <div id="page-container">            
            <div id="top-content">
                @section('top_content')
                @include('herotamer.partials.navi')
                <div id="right-top-content">
                    <div id="ingame-head"></div>
                    <div id="item-box">

                    </div> 
                    <div id="current-team-box">
                        @for($i = 0; $i < 5; $i++)
                            <div class="box"></div>
                        @endfor
                    </div>                   
                </div>        
                <div class="clearfix"></div>                         
                @show
            </div>
            <div id="bottom-content" style="position: relative;">
                @if(Session::has('modal'))
                    <div id="flyout"></div>
                @endif
                @yield('bottom_content')
            </div>
        </div>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="{{ asset('assets/js/vendor/jquery-ui-1.10.3.custom.min.js') }}"></script>        
        <script type="text/javascript" src="{{ asset('assets/js/vendor/jquery.powertip.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/vendor/jquery.nicescroll.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/startscript.js') }}"></script>  
        <!-- <script type="text/javascript" src="{{ asset('assets/js/vendor/live.js') }}"></script> --> 

        <script type="text/javascript">
            @if(Session::has('modal'))
                <script type="text/javascript">            
                    $(function(){ $('#dialog').dialog(); });
                </script>
            @endif
        </script>
        @yield('scripts')

    </body>
</html>
