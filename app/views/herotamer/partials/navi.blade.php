<div id="main-nav">
    <div id="nav-header">{{ isset($currentUser->username) ? $currentUser->username : '' }}</div>
    <ul id="main-nav-list">
        <li class="nav-dashboard"><a href="#">Dashboard</a></li>     
        <li class="nav-messages">
            {{ (isset($unread_messages) AND $unread_messages > 0) ? '<div class="circle-count">' . $unread_messages . '</div>' : '' }}
            <a href="{{ URL::to('messages/inbox') }}">Nachrichten</a>
        </li>     
        <li class="nav-team"><a href="#">Team</a></li>     
        <li class="nav-arena"><a href="#">Arena</a></li>     
        <li class="nav-world"><a href="#">Weltkarte</a></li>     
        <li class="nav-guild"><a href="#">Gilde</a></li>     
        <li class="nav-friends"><a href="#">Freunde</a></li>     
        <li class="nav-market"><a href="#">Markt</a></li>     
    </ul> 
</div> 