<div id="login-box">
<strong>Login</strong>
<br />
<br />
{{ Session::get('login_error') }}
{{ Form::open(array('url' => 'account/login')) }}
	{{ Form::text('login_email', null, array('placeholder' => 'E-Mail')) }}
	{{ Form::password('login_password', array('placeholder' => 'Passwort', 'style' => 'margin-top: 4px; margin-bottom: 10px;')) }}
	<br />
	<a href="#" class="tahoma no-ul" style="margin-top: 5px;">Passwort vergessen</a>
	{{ Form::submit('Einloggen', ['class' => 'login-submit'])}}
{{ Form::close() }}
</div>