@extends('herotamer.herotamer.index')

@section('bottom_content')
	<div id="book-content-ingame">
		<div id="page-headline"></div>

		<div id="inbox-wrapper-read">
			<div id="read-message-profile">
				<a id="read-message-profile-link" href="<?php echo URL::to('user/'.$message->user->id); ?>">
					<?php echo $message->user->username; ?><br />
					<span style="font-size: 10px;">Profil ansehen</span>
				</a>
			</div>
			<div id="read-message-right">
				<div id="read-message-subject-wrapper">
					<div id="read-message-subject">
						<?php echo $message->subject; ?>
					</div>
					<div id="read-message-date">
						<?php echo $message->created_at->format('H:i  d.m.Y'); ?>
					</div>
				</div>
				<div id="read-message-text">
					<div id="read-message-inner-text" class="cs-scroller">
						<?php echo nl2br(trim($message->text)); ?>
					</div>
				</div>
				<div id="read-message-actions">
					<a href="{{ URL::to('messages/redirect/'.$message->id) }}" class="message-action" id="pass-on"></a>
					<a href="{{ URL::to('messages/reply/'.$message->id) }}" class="message-action" id="reply"></a>					
				</div>
			</div>
		</div>
	</div>
@stop