@extends('herotamer.herotamer.index')

@section('bottom_content')
	<div id="book-content-ingame">
		<div id="page-headline"></div>

		<div id="inbox-wrapper">
			<div id="inbox-inner-messages">
				@foreach($messages AS $message)
					<a href="{{ URL::to('messages/inbox', array($message->id)) }}" class="inbox-message {{ $message->recipiant_flag == 0 ? 'unread' :''}}">
						<div class="inbox-inner-message">
							<div class="inbox-inner-item" style="width: 110px;">{{ $message->user->username }}</div>					
							<div class="inbox-inner-item" style="width: 351px;">{{ $message->subject }}</div>
							<div class="inbox-inner-item" style="width: 110px;">
								{{ $message->created_at->format('H:i') }}
								<div style="display: inline-block; margin-left: 12px;">{{ $message->created_at->format('d.m.Y') }}</div>
							</div>
						</div>
					</a>
				@endforeach
			</div>
			<div id="inbox-message-description">Verwalte hier deine Nachrichten. Klicke auf eine Nachricht um sie anzuzeigen</div>
			<a id="inbox-write-message" href="{{ URL::to('messages/write') }}"></a>
		</div>
	</div>
@stop