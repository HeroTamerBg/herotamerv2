@extends('herotamer.herotamer.index')

@section('bottom_content')
	<div id="book-content-ingame">
		<div id="page-headline"></div>
		@if($errors->any())
			@foreach($errors->all() AS $error)
				{{ $error }}
			@endforeach
		@endif
		{{ Form::open(array('url' => 'messages/send')) }}
			<div id="write-message-wrapper">
				<div id="write-header">
					<input type="text" name="subject" id="write-message-subject" placeholder="Betreff" {{ isset($message->subject) ? 'value="' . $message->subject . '"' : '' }} />
					<input type="text" name="to" id="write-message-receiver" placeholder="Empfänger" {{ isset($replyto) ? 'value="' . $replyto . '"' : '' }} />
				</div>
				<div id="write-message-textbox">
					<textarea {{ isset($message->text) ? : '' }} id="write-message-textbox" name="message" class="cs-scroller scroll-textarea">{{ isset($message->text) ? trim($message->text) : '' }}</textarea> 
				</div>
				<input type="submit" name="send_message" value="" />
			</div>
		{{ Form::close() }}
	</div>
@stop