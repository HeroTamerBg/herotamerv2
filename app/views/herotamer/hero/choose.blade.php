@extends('herotamer.herotamer.index')

@section('top_content')@stop

@section('bottom_content')
	<div id="choose-hero-wrapper">
		<div class="card active" id="warrior"></div>
		<div class="card active" id="mage"></div>
		<div class="card active" id="bow"></div>
		<div class="clearfix"></div>
		<a id="hero-url" href="#"></a>
	</div>
@stop

@section('scripts')
<script type="text/javascript">
	$(function(){
		var jobClass = null;

		$('.card').click(function(){
			$('.card').removeClass('active');
			$(this).addClass('active');
			jobClass = $(this).attr('id');

			$('#hero-url').attr('href', '{{ URL::to('hero/choose/') }}/'+jobClass);
		});
	});
</script
@stop
